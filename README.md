# README

Obrigado pelo tempo disposto para completar esse exercicio. Assim que completar seu exercício nos envio por email um arquivo *.zip* com sua solução e iremos marcar outra entrevista para um *peer review*.

### **Definição**
Esse pequeno projeto consite em uma pequena Dockerized RoR API com os seguintes models:
1. Person
2. Message
3. Company

- Uma Company possui multiplas Persons e uma Person pode possuir multiplas Messages.
- Uma Company deve possuir uma `api_key` que deverá ser utilizada nas APIs para separar as informações entre as Companies. (Garantir que uma empresa não vai acessar informações das outras)
- Uma Message deve possuir uma maneira de diferenciar o tipo de Message. `kind` (public|private).
- Uma Person deve possuir atributos como `name`, `mobile`, `document` e `enabled`.

### **Objetivos**

- Criar Restful APIs que irão permitir inserir, editar, listar e deletar Persons de uma determinada Company.
- Criar uma forma de separação dos dados das Companies de forma que uma Company não consiga visualizar os dados de outra Company utilizando o `api_key`.
- Criar Restful APIs, para inserção/edição de Persons
- Criar uma Restful, read-only API que irá permitir listar Persons com suas Messages em formato JSON, permitindo receber como `params`
os atributos criados para filtrar dados.
- Criar uma Restful API que irão permitir inserir Messages de uma determinada Person.
- Criar Specs do tipo `request` para testar as APIs criadas
- Algumas *gems* não foram adicionadas propositalmente

### **Bonus**

- Criar uma API que permitirá a partir de um arquivo .csv hospedado em algum site, importar uma lista de Persons em background utilizando Sidekiq.

### **Comandos Úteis**

- docker-compose run interview-api bundle install
- docker-compose up interview-api
- docker-compose run test bundle install
- docker-compose run test rails db:drop db:create db:migrate
- docker-compose run test

* Ruby version
*2.7.2*


