FROM ruby:2.7.2

RUN apt-get update && apt-get install --no-install-recommends -y \
      build-essential libpq-dev git patch unzip 

WORKDIR /app
RUN mkdir -p /app
COPY Gemfile* /app/
COPY . /app

# Run our app
ENTRYPOINT ["/app/entrypoint.sh"]
CMD ["bundle", "exec", "rails", "server", "-b", "0.0.0.0", "-p", "80"]


